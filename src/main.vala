/* main.vala
 *
 * Copyright 2022 Nathan Fisher
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Vapad {
    public string get_font_css (Pango.FontDescription font) {
        var family = font.get_family ();
        var style = "normal";
        switch (font.get_style ()) {
        case Pango.Style.ITALIC:
            style = "Italic";
            break;
        case Pango.Style.OBLIQUE:
            style = "Oblique";
            break;
        default:
            style = "Normal";
            break;
        }
        var size = font.get_size () / 1024;
        int weight = 400;
        switch (font.get_weight ()) {
        case Pango.Weight.BOLD:
            weight = 700;
            break;
        case Pango.Weight.THIN:
            weight = 100;
            break;
        case Pango.Weight.BOOK:
            weight = 400;
            break;
        case Pango.Weight.HEAVY:
            weight = 900;
            break;
        case Pango.Weight.LIGHT:
            weight = 300;
            break;
        case Pango.Weight.MEDIUM:
            weight = 500;
            break;
        case Pango.Weight.SEMIBOLD:
            weight = 600;
            break;
        case Pango.Weight.SEMILIGHT:
            weight = 350;
            break;
        case Pango.Weight.ULTRABOLD:
            weight = 800;
            break;
        case Pango.Weight.ULTRALIGHT:
            weight = 100;
            break;
        case Pango.Weight.ULTRAHEAVY:
            weight = 950;
            break;
        default:
            weight = 400;
            break;
        }
        var css = @"textview.view { font-family: $family; font-size: $(size)pt; font-weight: $weight; font-style: $style; }";
        return css;
    }

    public void set_editor_font (Pango.FontDescription font) {
        string css = Vapad.get_font_css (font);
        Gtk.CssProvider provider = new Gtk.CssProvider ();
        provider.load_from_data (css.data);
        Gtk.StyleContext.add_provider_for_display (Gdk.Display.get_default (), provider, Gtk.STYLE_PROVIDER_PRIORITY_USER);
    }
}

int main (string[] args) {
    Intl.bindtextdomain (Config.GETTEXT_PACKAGE, Config.LOCALEDIR);
    Intl.bind_textdomain_codeset (Config.GETTEXT_PACKAGE, "UTF-8");
    Intl.textdomain (Config.GETTEXT_PACKAGE);
    var app = new Vapad.Application ();
    return app.run (args);
}
